`npm i`

# mock api server
`nodemon -L --watch 'index.js' index.js`

# mimic client
`nodemon -L --watch 'mockClient.js' mockClient.js`

# Rabbit MQ

Start
```
docker run -d --hostname my-rabbit -p 15672:15672 -p 5672:5672 --name rabbitmq rabbitmq:3-management
```

# UI

[readme.md](webui/readme.md)

## Python3
`(cd webui && python3 -m http.server 8001)`

## Python2
`(cd webui && python -m SimpleHTTPServer 8001)`