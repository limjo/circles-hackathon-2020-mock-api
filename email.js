const sgMail = require('@sendgrid/mail');
const emailToName = require('email-to-name')

const sender = 'cut@circles.asia'; //Don't change

let msgBill = {
    from: sender, // Change to your verified sender
}

const msgNotif = {
    from: sender, // Change to your verified sende
}

class Email {
    constructor() {
        sgMail.setApiKey('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');
    }

    async sendBillEmail(receiver, amount = 1000) {
        return sgMail.send({
            ...msgBill,
            to: receiver,
            subject: `Hi ${emailToName.process(receiver)}, another month has passed!`,
            text: "Here is your bill for the month. Have a great month ahead!",
            html: "Here is your bill for the month. Have a great month ahead!"
        })
    }

    sendPaymentNotification(receiver, amount = 1000) {
        sgMail
            .send({
                ...msgNotif,
                to: receiver,
                text: `Hi ${emailToName.process(receiver)}, Your payment has been processed :) It\'s been a joy serving you`,
                html: `Hi ${emailToName.process(receiver)}, Your payment has been processed :) It\'s been a joy serving you`,
                subject: `Thanks for journeying with us!`
            })
            .then(() => {
                console.log('Email sent')
            })
            .catch((error) => {
                console.error(error)
            })
    }
}

module.exports = Email;