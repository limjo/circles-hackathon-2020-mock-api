const express = require('express')
const app = express()
const port = 3002
const axios = require('axios')

const server = 'http://localhost:3001'
const deferred = {
}

const FETCH_BRM_1 = "FETCH_BRM_1"
const INVOICE_DISPATCH_1 = "INVOICE_DISPATCH_1"

const promises = [FETCH_BRM_1, INVOICE_DISPATCH_1].map(job_identifier => {
    return new Promise((resolve, reject) => {
        deferred[job_identifier] = { resolve: () => {
            console.log(`Job: ${job_identifier} completed`)
            resolve()
        }, reject }
    })
})

app.get('/set_job_status', (req, res) => {
    const {job_identifier, status} = req.query
    console.log(`received ${job_identifier}: status ${status}`)
    deferred[job_identifier].resolve()
    res.send(`callback ack: ${job_identifier} ${status}`)
})

const me = `http://localhost:${port}`
const client = app.listen(port, () => {
    console.log(`Mock client app listening at ${me}`)
})

async function main() {

    console.log("client trigger fetch brm files")
    await axios({
        url: `${server}/fetch-files-from-brm-to-billing-fs`,
        params: {
            callback: `${me}/set_job_status?job_identifier=${FETCH_BRM_1}&status=complete`
        }
    })

    console.log("client trigger invoice dispatch")
    await axios({
        url: `${server}/invoice-dispatch`,
        params: {
            callback: `${me}/set_job_status?job_identifier=${INVOICE_DISPATCH_1}&status=complete`
        }
    })

    await Promise.all(promises)

    client.close(() => {
        console.log("stopped server")
    })
}

main()