const express = require('express')
const app = express()
const port = 3001
const axios = require('axios');
const Email = require('./email');
const emailSender = new Email();

function sleep(time, callback) {
  setTimeout(callback, time)
}

app.get('/', (_, res) => {
  console.log("request received")
  res.send('hello')
})

app.get('/fetch-files-from-brm-to-billing-fs', (req, res) => {
  const time = 10000
  console.log(`triggered fetch brm files will finish in ${time / 1000} seconds`)
  const { callback } = req.query
  sleep(time, async () => {
    console.log(`finished brm sending hook ${callback}`)
    const { data } = await axios.get(callback)
    console.log(`${data}`)
  })
  res.send()
})

app.get('/invoice-dispatch', (req, res) => {
  console.log("triggered invoice dispatch")
  const { callback } = req.query
  sleep(3000, async () => {
    console.log(`finished dispatch sending hook ${callback}`)
    if (callback) {
      const { data } = await axios.get(callback)
      console.log(`${data}`)
    }
  })
  res.send()
})

app.get('/email', async (req, res) => {
  console.log("triggered email")
  const { callback, email } = req.query
  await emailSender.sendBillEmail(email, 10000);
  sleep(3000, async () => {
    console.log(`finished email sending hook ${callback}`)
    if (callback) {
      const { data } = await axios.get(callback)
      console.log(`${data}`)
    }
  })
  res.send()
})

app.get('/payment', async (req, res) => {
  console.log("triggered email")
  const { callback, email } = req.query
  await emailSender.sendPaymentNotification(email, 10000);
  sleep(3000, async () => {
    console.log(`finished payment email sending hook ${callback}`)
    if (callback) {
      const { data } = await axios.get(callback)
      console.log(`${data}`)
    }
  })
  res.send()
})

/**
 * All HTTP GET requests to this route can have `callback` query which will be called after
 * `finishAfter`
 * @param {*} app express app
 * @param {*} method HTTP method
 * @param {*} route HTTP path
 * @param {*} finishAfter seconds
 * @param {*} doWhat callback
 */
function generateRoute(app, method, route, finishAfter, doWhat) {
  app[method](route, (req, res) => {
    console.log(`triggered ${route} will finish in ${finishAfter} seconds`)
    const { callback } = req.query || {}
    const sw = StopWatch()
    s
    sleep(3000, async () => {
      if (callback) {
        console.log(`finished dispatch ${route} ${req.query} calling hook ${callback}`)
        const { data } = await axios.get(callback)
        console.log(`${data}`)
      }
      doWhat(req)
    })
    res.send('request received')
  })
}

generateRoute(app, 'get', '/generated-route', 3, (req, elapsed) => {
  console.log(`the request ${req} sent awhile ago has finished in ${elapsed} seconds`)
})

app.listen(port, () => {
  console.log(`Mock Server app listening at http://localhost:${port}`)
})

async function testInit() {
  emailSender.sendBillEmail('joel.lim@circles.asia', 1000);
  emailSender.sendPaymentNotification('joel.lim@circles.asia', 1000);
}

testInit()